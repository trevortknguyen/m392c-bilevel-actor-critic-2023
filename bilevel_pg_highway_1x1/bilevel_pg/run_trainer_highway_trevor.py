# Created by yingwen at 2019-03-16
import gym
import highway_env

from bilevel_pg.bilevelpg.utils.random import set_seed
from bilevel_pg.bilevelpg.logger.utils import set_logger
from bilevel_pg.bilevelpg.policy.base_policy import StochasticMLPPolicy
from bilevel_pg.bilevelpg.value_functions import MLPValueFunction
from bilevel_pg.bilevelpg.replay_buffers_highway import IndexedReplayBuffer
import numpy as np
import tensorflow as tf
import numpy as np
level_agent_num = 2

from bilevel_pg.bilevelpg.agents.base_agents_highway import OffPolicyAgent
from bilevel_pg.bilevelpg.core import Serializable
from bilevel_pg.bilevelpg.utils import tf_utils

from bilevel_pg.bilevelpg.logger import logger, tabular
from highway_env import utils
num_sample = 10

class Sampler(object):
    def __init__(self, max_path_length, min_pool_size, batch_size):
        self._max_path_length = max_path_length
        self._min_pool_size = min_pool_size
        self._batch_size = batch_size

        self.env = None
        self.policy = None
        self.pool = None

    def initialize(self, env, policy, pool):
        self.env = env
        self.policy = policy
        self.pool = pool

    def set_policy(self, policy):
        self.policy = policy

    def sample(self):
        raise NotImplementedError

    def batch_ready(self):
        enough_samples = self.pool.size >= self._min_pool_size
        return enough_samples

    def random_batch(self):
        return self.pool.random_batch(self._batch_size)

    def terminate(self):
        self.env.terminate()

    def log_diagnostics(self):
        logger.record_tabular('pool-size', self.pool.size)


class MASampler(Sampler):
    def __init__(self, agent_num, leader_num, follower_num, leader_action_num = 2, max_path_length=20, min_pool_size=10e4, batch_size=64, global_reward=False, **kwargs):
        # super(MASampler, self).__init__(**kwargs)
        self.agent_num = agent_num
        self.leader_num = leader_num
        self.leader_action_num = leader_action_num
        self.follower_num = follower_num
        self._max_path_length = max_path_length
        self._min_pool_size = min_pool_size
        self._batch_size = batch_size
        self._global_reward = global_reward
        self._path_length = 0
        self._path_return = np.zeros(self.agent_num)
        self._last_path_return = np.zeros(self.agent_num)
        self._max_path_return = np.array([-np.inf] * self.agent_num, dtype=np.float32)
        self._n_episodes = 0
        self._total_samples = 0
        self.step = 0
        self._current_observation_n = None
        self.env = None
        self.agents = None
        self.count = 0
        self.level_agent_num = 2
        self.leader_idx = 0
        self.follower_idx = 1
        self.correct_merge = 0
        self.idle_action = 0
        self.rewards_record = []

    def set_policy(self, policies):
        for agent, policy in zip(self.agents, policies):
            agent.policy = policy

    def batch_ready(self):
        enough_samples = max(agent.replay_buffer.size for agent in self.agents) >= self._min_pool_size
        return enough_samples

    def random_batch(self, i):
        return self.agents[i].pool.random_batch(self._batch_size)

    def initialize(self, env, agents, train_agents):
        self._current_observation_n = None
        self.env = env
        self.agents = agents
        self.train_agents = train_agents

    def sample(self, explore=False):
        self.step += 1
        # print(self._current_observation_n)
        
        if self._current_observation_n is None:
            # print('now updating')
            self._current_observation_n = self.env.reset()
        
        action_n = []
        supplied_observation = []

        observations = np.zeros((2, self.env.num_state))
        next_observations = np.zeros((2, self.env.num_state))
        if self.env.sim_step >= self.env.num_state - 3:
            print('wrong')
        observations[0][self.env.sim_step] = 1
        observations[1][self.env.sim_step] = 1
        next_observations[0][self.env.sim_step + 1] = 1
        next_observations[1][self.env.sim_step + 1] = 1
        relative_info = np.zeros((2, 2))
        speed_max = 40
        velocity_range = 2 * 40
        x_position_range = speed_max
        delta_dx = self.env.road.vehicles[1].position[0] - self.env.road.vehicles[0].position[0]
        delta_vx = self.env.road.vehicles[1].velocity - self.env.road.vehicles[0].velocity 
        relative_info[0][0] = utils.remap(delta_dx, [-x_position_range, x_position_range], [-1, 1])
        relative_info[0][1] = utils.remap(delta_vx, [-velocity_range, velocity_range], [-1, 1])
        relative_info[1][0] = -relative_info[0][0]
        relative_info[1][1] = -relative_info[0][1]
        observations[:, -2:] = relative_info
        if explore:
            for i in range(self.agent_num):
                action_n.append([np.random.randint(0, self.env.action_num)])
            for i in range(self.leader_num):
                supplied_observation.append(observations[i])
            for i in range(self.leader_num, self.env.agent_num):      
                supplied_observation.append(np.hstack((observations[i],  tf.one_hot(action_n[0][0], self.env.action_num))).reshape(1, -1))
        else:
            for i in range(self.leader_num):
                supplied_observation.append(observations[i])
                action_n.append(self.train_agents[0].act(observations[i].reshape(1, -1)))
            for i in range(self.leader_num, self.env.agent_num):
                mix_obs = np.hstack((observations[i], tf.one_hot(action_n[0][0], self.env.action_num))).reshape(1, -1)
                supplied_observation.append(mix_obs)
                follower_action = self.train_agents[1].act(mix_obs.reshape(1, -1))
                action_n.append(follower_action)
                
        action_n = np.asarray(action_n)
        #self.env.render()
        next_observation_n, reward_n, done_n, info = self.env.step(action_n)
        delta_dx = self.env.road.vehicles[1].position[0] - self.env.road.vehicles[0].position[0]
        delta_vx = self.env.road.vehicles[1].velocity - self.env.road.vehicles[0].velocity 
        relative_info[0][0] = utils.remap(delta_dx, [-x_position_range, x_position_range], [-1, 1])
        relative_info[0][1] = utils.remap(delta_vx, [-velocity_range, velocity_range], [-1, 1])
        relative_info[1][0] = -relative_info[0][0]
        relative_info[1][1] = -relative_info[0][1]
        next_observations[:, -2:] = relative_info

        if self._global_reward:
            reward_n = np.array([np.sum(reward_n)] * self.agent_num)

        self._path_length += 1
        self._path_return += np.array(reward_n, dtype=np.float32)
        self._total_samples += 1
        opponent_action = np.array(action_n[[j for j in range(len(action_n))]].flatten())
        for i, agent in enumerate(self.agents):            
            agent.replay_buffer.add_sample(
                # observation=self._current_observation_n[i].astype(np.float32),
                observation=supplied_observation[i],
                # action=action_n[i].astype(np.float32),
                action = tf.one_hot(action_n[i], self.env.action_num),
                # reward=reward_n[i].astype(np.float32),
                reward = np.float32(reward_n[i]),
                # terminal=done_n[i].astype(np.float32),
                terminal=np.float32(done_n[i]),
                # next_observation=next_observation_n[i].astype(np.float32),
                next_observation=np.float32(next_observations[i]),
                # opponent_action=opponent_action.astype(np.float32)
                opponent_action=np.int32(opponent_action),
            )
        
             
        self._current_observation_n = next_observation_n
        if self.step % (25 * 1000) == 0:
            print("steps: {}, episodes: {}, mean episode reward: {}".format(
                        self.step, len(reward_n), np.mean(reward_n[-1000:])))

        if np.all(done_n) or self._path_length >= self._max_path_length:
            self._current_observation_n = self.env.reset()
            self._max_path_return = np.maximum(self._max_path_return, self._path_return)
            self._mean_path_return = self._path_return / self._path_length
            self._last_path_return = self._path_return
            self._path_length = 0

            self._path_return = np.zeros(self.agent_num)
            self._n_episodes += 1
            self.log_diagnostics()
            logger.log(tabular)
            logger.dump_all()
        else:
            self._current_observation_n = next_observation_n

    def log_diagnostics(self):
        for i in range(self.agent_num):
            tabular.record('max-path-return_agent_{}'.format(i), self._max_path_return[i])
            tabular.record('mean-path-return_agent_{}'.format(i), self._mean_path_return[i])
            tabular.record('last-path-return_agent_{}'.format(i), self._last_path_return[i])
        tabular.record('episodes', self._n_episodes)
        tabular.record('episode_reward', self._n_episodes)
        tabular.record('total-samples', self._total_samples)

class RLAgent:
    def act(self, observation):
        raise NotImplementedError

    def train(self):
        raise NotImplementedError


class OffPolicyAgent(RLAgent, Serializable):
    """This class implements OffPolicyRLAgents."""

    def __init__(
            self,
            observation_space,
            action_space,
            policy,
            qf,
            replay_buffer,
            train_sequence_length, # for rnn policy
            name='off_policy_agent',
            *args,
            **kwargs):
        self._Serializable__initialize(locals())
        self._observation_space = observation_space
        self._action_space = action_space
        self._policy = policy
        self._qf = qf
        self._replay_buffer = replay_buffer
        self._train_sequence_length = train_sequence_length
        self._name = name
        self.init_opt()

    def log_diagnostics(self, batch):
        """Log diagnostic information on current batch."""
        self.policy.log_diagnostics(batch)
        self.qf.log_diagnostics(batch)

    def init_opt(self):
        """
        Initialize the optimization procedure.
        If using tensorflow, this may
        include declaring all the variables and compiling functions.
        """
        raise NotImplementedError

    def init_eval(self):
        """
        Initialize the evaluation procedure.
        """
        raise NotImplementedError


    @property
    def action_space(self):
        return self._action_space

    @property
    def observation_space(self):
        return self._observation_space

    @property
    def replay_buffer(self):
        return self._replay_buffer

    @property
    def policy(self):
        """Return the current policy held by the agent.
        Returns:
          A Policy object.
        """
        return self._policy

    @property
    def qf(self):
        """Return the current policy held by the agent.
        Returns:
          A Policy object.
        """
        return self._qf

    @property
    def train_sequence_length(self):
        return self._train_sequence_length

    def train(self, batch, env, agent_id, weights=None):
        if self.train_sequence_length is not None:
            if batch['observations'].shape[1] != self.train_sequence_length:
                raise ValueError('Invalid sequence length in batch.')
        loss_info = self._train(batch=batch, env=env, agent_id=agent_id, weights=weights)
        return loss_info

    def _train(self, batch, env, agent_id, weights):
        raise NotImplementedError

class LeaderAgent(OffPolicyAgent):
    def __init__(self,
                 env_specs,
                 policy,
                 qf,
                 replay_buffer,
                 policy_optimizer=tf.keras.optimizers.legacy.Adam(lr=0.001),
                 qf_optimizer=tf.keras.optimizers.legacy.Adam(lr=0.01),
                 exploration_strategy=None,
                 exploration_interval=10,
                 target_update_tau=0.01,
                 target_update_period=10,
                 td_errors_loss_fn=None,
                 gamma=1,
                 reward_scale=1.0,
                 gradient_clipping=None,
                 train_sequence_length=None,
                 name='Bilevel_leader',
                 agent_id=-1
                 ):
        self._Serializable__initialize(locals())
        self._agent_id = agent_id
        self._env_specs = env_specs
        if self._agent_id >= 0:
            observation_space = self._env_specs.observation_space[self._agent_id]
            action_space = self._env_specs.action_space[self._agent_id]
        else:
            observation_space = self._env_specs.observation_space
            action_space = self._env_specs.action_space

        self._exploration_strategy = exploration_strategy

        self._target_policy = Serializable.clone(policy, name='target_policy_agent_{}'.format(self._agent_id))
        self._target_qf = Serializable.clone(qf, name='target_qf_agent_{}'.format(self._agent_id))

        self._policy_optimizer = policy_optimizer
        self._qf_optimizer = qf_optimizer

        self._target_update_tau = target_update_tau
        self._target_update_period = target_update_period
        self._td_errors_loss_fn = (
                td_errors_loss_fn or tf.losses.Huber)
        self._gamma = gamma
        self._reward_scale = reward_scale
        self._gradient_clipping = gradient_clipping
        self._train_step = 0
        self._exploration_interval = exploration_interval
        self._exploration_status = False

        self.required_experiences = ['observation', 'actions', 'rewards', 'next_observations',
                                     'opponent_actions', 'target_actions']

        super(LeaderAgent, self).__init__(
            observation_space,
            action_space,
            policy,
            qf,
            replay_buffer,
            train_sequence_length=train_sequence_length,
            name=name,
        )

    def get_policy_np(self,
                         input_tensor):
        return self._policy.get_policy_np(input_tensor)

    def act(self, observation, step=None, use_target=False):
        if use_target and self._target_policy is not None:
            return self._target_policy.get_actions_np(observation)
        policy = self._policy

        return policy.get_actions_np(observation)

    def init_opt(self):
        tf_utils.soft_variables_update(
            self._policy.trainable_variables,
            self._target_policy.trainable_variables,
            tau=1.0)
        tf_utils.soft_variables_update(
            self._qf.trainable_variables,
            self._target_qf.trainable_variables,
            tau=1.0)
        self._exploration_status = True

    def init_eval(self):
        self._exploration_status = False

    def _update_target(self):
        tf_utils.soft_variables_update(
            self._policy.trainable_variables,
            self._target_policy.trainable_variables,
            tau=self._target_update_tau)
        tf_utils.soft_variables_update(
            self._qf.trainable_variables,
            self._target_qf.trainable_variables,
            tau=self._target_update_tau)

    def _train(self, batch, env, agent_id, weights=None):
        critic_variables = self._qf.trainable_variables
        with tf.GradientTape(watch_accessed_variables=False) as tape:
            assert critic_variables, 'No qf variables to optimize.'
            tape.watch(critic_variables)
            critic_loss = self.critic_loss(env,
                                           agent_id,
                                           batch['observations'],
                                           batch['actions'],
                                           batch['opponent_actions'],
                                           batch['target_actions'],
                                           batch['rewards'],
                                           batch['next_observations'],
                                           batch['terminals'],
                                           weights=weights)
        tf.debugging.check_numerics(critic_loss, 'qf loss is inf or nan.')
        critic_grads = tape.gradient(critic_loss, critic_variables)
        tf_utils.apply_gradients(critic_grads, critic_variables, self._qf_optimizer, self._gradient_clipping)

        actor_variables = self._policy.trainable_variables
        with tf.GradientTape(watch_accessed_variables=False) as tape:
            assert actor_variables, 'No actor variables to optimize.'
            tape.watch(actor_variables)
            actor_loss = self.actor_loss(env, agent_id, batch['observations'], batch['opponent_actions'], weights=weights)
        tf.debugging.check_numerics(actor_loss, 'Actor loss is inf or nan.')
        actor_grads = tape.gradient(actor_loss, actor_variables)
        tf_utils.apply_gradients(actor_grads, actor_variables, self._policy_optimizer, self._gradient_clipping)
        self._train_step += 1

        if self._train_step % self._target_update_period == 0:
            self._update_target()

        losses = {
            'pg_loss': actor_loss.numpy(),
            'critic_loss': critic_loss.numpy(),
        }

        return losses

    def get_critic_value(self,
                         input_tensor):
        return self._qf.get_values(input_tensor)

    def critic_loss(self,
                    env,
                    agent_id,
                    observations,
                    actions,
                    opponent_actions,
                    target_actions,
                    rewards,
                    next_observations,
                    terminals,
                    weights=None):
        """Computes the critic loss for DDPG training.
        Args:
          observations: A batch of observations.
          actions: A batch of actions.
          rewards: A batch of rewards.
          next_observations: A batch of next observations.
          weights: Optional scalar or element-wise (per-batch-entry) importance
            weights.
        Returns:
          critic_loss: A scalar critic loss.
        """
        action_num = env.action_num
        level_agent_num = env.level_agent_num
        num_state = env.num_state
        agent_num = target_actions.shape[1]
        batch_size = target_actions.shape[0]
        target_actions_concat = np.zeros((batch_size, 2 * action_num))
        actions_concat = np.zeros((batch_size, 2 * action_num))
        
        target_actions_concat[:, :action_num] = tf.one_hot(target_actions[:, agent_id], action_num)
        actions_concat[:, :action_num] = actions
        opponent_actions = np.int32(opponent_actions)
        target_actions_concat[:, action_num:] = tf.one_hot(target_actions[:, 1 - agent_id], action_num)
        actions_concat[:, action_num:] = tf.one_hot(opponent_actions[:, 1 - agent_id], action_num)
        target_critic_input = np.zeros((batch_size, num_state + 2 * action_num))
        target_critic_input[:, :num_state] = next_observations
        target_critic_input[:, num_state:] = target_actions_concat

        target_q_values = self._target_qf.get_values(target_critic_input)

        rewards = rewards.reshape(-1, 1)
        td_targets = tf.stop_gradient(
                self._reward_scale * rewards + (1 - terminals.reshape(-1, 1)) * self._gamma * target_q_values)

        critic_net_input = np.hstack((observations, actions_concat))
        q_values = self._qf.get_values(critic_net_input)

        critic_loss = self._td_errors_loss_fn(reduction=tf.losses.Reduction.NONE)(td_targets, q_values)

        if weights is not None:
            critic_loss = weights * critic_loss

        critic_loss = tf.reduce_mean(critic_loss)
        return critic_loss

    def actor_loss(self, env, agent_id, observations, opponent_actions, weights=None):
        """Computes the actor_loss for DDPG training.
        Args:
          observations: A batch of observations.
          weights: Optional scalar or element-wise (per-batch-entry) importance
            weights.
          # TODO: Add an action norm regularizer.
        Returns:
          actor_loss: A scalar actor loss.
        """
        batch_size = observations.shape[0]
        policies = self._policy.get_policies(observations)
        # print(policies.shape[1])
        tot_q_values = None
        actions_concat = np.zeros((batch_size, 2 * env.action_num))
        opponent_actions = np.int32(opponent_actions)
        actions_concat[:, env.action_num:] = tf.one_hot(opponent_actions[:, 1 - agent_id], env.action_num)
        
        for action in range(policies.shape[1]):
            # print(tf.shape(observations)[0])
            actions = tf.fill([tf.shape(observations)[0]], action)
            actions = tf.one_hot(actions, self.action_space.n)
            actions_concat[:, :env.action_num] = actions
            q_values = self._qf.get_values(tf.concat((observations, actions_concat), 1))
            if tot_q_values == None:
                tot_q_values = tf.multiply(policies[:,action:action+1], q_values)
            else:
                tot_q_values += tf.multiply(policies[:,action:action+1], q_values)

        actor_loss = -tf.reduce_mean(tot_q_values)
        # print(actor_loss, 'actor_loss')
        return actor_loss

class FollowerAgent(OffPolicyAgent):
    def __init__(self,
                 env_specs,
                 policy,
                 qf,
                 replay_buffer,
                 policy_optimizer=tf.keras.optimizers.legacy.Adam(lr=0.001),
                 qf_optimizer=tf.keras.optimizers.legacy.Adam(lr=0.01),
                 exploration_strategy=None,
                 exploration_interval=10,
                 target_update_tau=0.01,
                 target_update_period=10,
                 td_errors_loss_fn=None,
                 gamma=1,
                 reward_scale=1.0,
                 gradient_clipping=None,
                 train_sequence_length=None,
                 name='Bilevel_follower',
                 agent_id=-1
                 ):
        self._Serializable__initialize(locals())
        self._agent_id = agent_id
        self._env_specs = env_specs
        if self._agent_id >= 0:
            observation_space = self._env_specs.observation_space[self._agent_id]
            action_space = self._env_specs.action_space[self._agent_id]
        else:
            observation_space = self._env_specs.observation_space
            action_space = self._env_specs.action_space

        self._exploration_strategy = None

        self._target_policy = Serializable.clone(policy, name='target_policy_agent_{}'.format(self._agent_id))
        self._target_qf = Serializable.clone(qf, name='target_qf_agent_{}'.format(self._agent_id))

        self._policy_optimizer = policy_optimizer
        self._qf_optimizer = qf_optimizer

        self._target_update_tau = target_update_tau
        self._target_update_period = target_update_period
        self._td_errors_loss_fn = (
                td_errors_loss_fn or tf.losses.Huber)
        self._gamma = gamma
        self._reward_scale = reward_scale
        self._gradient_clipping = gradient_clipping
        self._train_step = 0
        self._exploration_interval = exploration_interval
        self._exploration_status = False

        self.required_experiences = ['observation', 'actions', 'rewards', 'next_observations',
                                     'opponent_actions', 'target_actions']

        super(FollowerAgent, self).__init__(
            observation_space,
            action_space,
            policy,
            qf,
            replay_buffer,
            train_sequence_length=train_sequence_length,
            name=name,
        )

    def get_policy_np(self,
                         input_tensor):
        return self._policy.get_policy_np(input_tensor)

    def act(self, observation, step=None, use_target=False):
        if use_target and self._target_policy is not None:
            return self._target_policy.get_actions_np(observation)
        policy = self._policy

        return policy.get_actions_np(observation)

    def init_opt(self):
        tf_utils.soft_variables_update(
            self._policy.trainable_variables,
            self._target_policy.trainable_variables,
            tau=1.0)
        tf_utils.soft_variables_update(
            self._qf.trainable_variables,
            self._target_qf.trainable_variables,
            tau=1.0)
        self._exploration_status = True

    def init_eval(self):
        self._exploration_status = False

    def _update_target(self):
        tf_utils.soft_variables_update(
            self._policy.trainable_variables,
            self._target_policy.trainable_variables,
            tau=self._target_update_tau)
        tf_utils.soft_variables_update(
            self._qf.trainable_variables,
            self._target_qf.trainable_variables,
            tau=self._target_update_tau)

    def _train(self, batch, env, agent_id, weights=None):
        critic_variables = self._qf.trainable_variables
        with tf.GradientTape(watch_accessed_variables=False) as tape:
            assert critic_variables, 'No qf variables to optimize.'
            tape.watch(critic_variables)
            critic_loss = self.critic_loss(env,
                                           agent_id,
                                           batch['observations'],
                                           batch['actions'],
                                           batch['opponent_actions'],
                                           batch['target_actions'],
                                           batch['rewards'],
                                           batch['next_observations'],
                                           batch['terminals'],
                                           weights=weights)
        tf.debugging.check_numerics(critic_loss, 'qf loss is inf or nan.')
        critic_grads = tape.gradient(critic_loss, critic_variables)
        tf_utils.apply_gradients(critic_grads, critic_variables, self._qf_optimizer, self._gradient_clipping)

        actor_variables = self._policy.trainable_variables
        with tf.GradientTape(watch_accessed_variables=False) as tape:
            assert actor_variables, 'No actor variables to optimize.'
            tape.watch(actor_variables)
            actor_loss = self.actor_loss(env, agent_id, batch['observations'], batch['opponent_actions'], weights=weights)
        tf.debugging.check_numerics(actor_loss, 'Actor loss is inf or nan.')
        actor_grads = tape.gradient(actor_loss, actor_variables)
        tf_utils.apply_gradients(actor_grads, actor_variables, self._policy_optimizer, self._gradient_clipping)
        self._train_step += 1

        if self._train_step % self._target_update_period == 0:
            self._update_target()

        losses = {
            'pg_loss': actor_loss.numpy(),
            'critic_loss': critic_loss.numpy(),
        }

        return losses

    def get_critic_value(self,
                         input_tensor):
        return self._qf.get_values(input_tensor)

    def critic_loss(self,
                    env,
                    agent_id,
                    observations,
                    actions,
                    opponent_actions,
                    target_actions,
                    rewards,
                    next_observations,
                    terminals,
                    weights=None):
        """Computes the critic loss for DDPG training.
        Args:
          observations: A batch of observations.
          actions: A batch of actions.
          rewards: A batch of rewards.
          next_observations: A batch of next observations.
          weights: Optional scalar or element-wise (per-batch-entry) importance
            weights.
        Returns:
          critic_loss: A scalar critic loss.
        """
        action_num = env.action_num
        level_agent_num = 2
        num_state = next_observations.shape[1]
        batch_size = target_actions.shape[0]
        target_actions_concat = np.zeros((batch_size, 2 * action_num))
        actions_concat = np.zeros((batch_size, 2 * action_num))
        
        target_actions_concat[:, :action_num] = tf.one_hot(target_actions[:, agent_id], action_num)
        actions_concat[:, :action_num] = actions
        opponent_actions = np.int32(opponent_actions)
        target_actions_concat[:, action_num:] = tf.one_hot(target_actions[:, 1 - agent_id], action_num)
        actions_concat[:, action_num:] = tf.one_hot(opponent_actions[:, 1 - agent_id], action_num)
        target_critic_input = np.zeros((batch_size, num_state + 2 * action_num))
        target_critic_input[:, :num_state] = next_observations
        target_critic_input[:, num_state:] = target_actions_concat

        target_q_values = self._target_qf.get_values(target_critic_input)

        rewards = rewards.reshape(-1, 1)
        td_targets = tf.stop_gradient(
                self._reward_scale * rewards + (1 - terminals.reshape(-1, 1)) * self._gamma * target_q_values)

        critic_net_input = np.hstack((observations[:, :num_state], actions_concat))
        q_values = self._qf.get_values(critic_net_input)

        critic_loss = self._td_errors_loss_fn(reduction=tf.losses.Reduction.NONE)(td_targets, q_values)

        if weights is not None:
            critic_loss = weights * critic_loss

        critic_loss = tf.reduce_mean(critic_loss)
        return critic_loss

    def actor_loss(self, env, agent_id, observations, opponent_actions, weights=None):
        """Computes the actor_loss for DDPG training.
        Args:
          observations: A batch of observations.
          weights: Optional scalar or element-wise (per-batch-entry) importance
            weights.
          # TODO: Add an action norm regularizer.
        Returns:
          actor_loss: A scalar actor loss.
        """
        batch_size = observations.shape[0]
        policies = self._policy.get_policies(observations)
        # print(policies.shape[1])
        tot_q_values = None
        actions_concat = np.zeros((batch_size, 2 * env.action_num))
        opponent_actions = np.int32(opponent_actions)
        actions_concat[:, env.action_num:] = tf.one_hot(opponent_actions[:, 1 - agent_id], env.action_num)
        
        for action in range(policies.shape[1]):
            # print(tf.shape(observations)[0])
            actions = tf.fill([tf.shape(observations)[0]], action)
            actions = tf.one_hot(actions, env.action_num)
            actions_concat[:, :env.action_num] = actions
            q_values = self._qf.get_values(tf.concat((observations.astype(np.float32)[:, :env.num_state], actions_concat), 1))
            if tot_q_values == None:
                tot_q_values = tf.multiply(policies[:,action:action+1], q_values)
            else:
                tot_q_values += tf.multiply(policies[:,action:action+1], q_values)

        actor_loss = -tf.reduce_mean(tot_q_values)
        # print(actor_loss, 'actor_loss')
        return actor_loss

"""
The trainer for multi-agent training.
"""
import pickle
from bilevel_pg.bilevelpg.trainer.utils_highway import *
import time
import numpy as np

class Bilevel_Trainer:
    """This class implements a multi-agent trainer.
    """
    def __init__(
            self, seed, env, agents, train_agents, sampler,
            batch_size=128,
            steps=10000,
            exploration_steps=100,
            training_interval=1,
            extra_experiences=['target_actions'],
            save_path=None,
    ):
        self.env = env
        self.seed = seed
        self.agents = agents
        self.train_agents = train_agents
        self.sampler = sampler
        self.batch_size = batch_size
        self.steps = steps
        self.exploration_steps = exploration_steps
        self.training_interval = training_interval
        self.extra_experiences = extra_experiences
        self.losses = []
        self.save_path = save_path
        self.epsilon = 0.1
        self.success_rate = []

    def setup(self, env, agents, sampler):
        self.env = env
        self.agents = agents
        self.sampler = sampler

    def sample_batches(self):
        assert len(self.agents) > 1
        indices = self.agents[0].replay_buffer.random_indices(self.batch_size)
        return [
            agent.replay_buffer.batch_by_indices(indices)
            for agent in self.agents
        ]

    def do_communication(self):
        pass

    def individual_forward(self):
        pass

    def centralized_forward(self):
        pass

    def apply_gradient(self):
        pass

    def run(self):
        #print('trainer_start')
        for step in range(self.steps):
            if step < self.exploration_steps:
                 self.sampler.sample(explore=True)
                 continue
            
            if (np.random.uniform(0, 1) < self.env.epsilon):
                self.sampler.sample(explore=True)
            else:
                self.sampler.sample()
            
            
            batches = self.sample_batches()

            for extra_experience in self.extra_experiences:
                if extra_experience == 'annealing':
                    batches = add_annealing(batches, step, annealing_scale=1.)
                elif extra_experience == 'target_actions':
                    batches = add_target_actions(self.env, self.sampler, batches, self.agents, self.train_agents, self.batch_size)
                elif extra_experience == 'recent_experiences':
                    batches = add_recent_batches(batches, self.agents, self.batch_size)
            agents_losses = []

            # print('extra finish')
            # print(int(round(time.time() * 1000)))

            if step % self.training_interval == 0:
                
                for agent, batch in zip(self.agents, batches):
                    if agent._agent_id < self.sampler.leader_num:
                        agent_losses = self.train_agents[0].train(batch, self.env, agent._agent_id)
                    else:
                        agent_losses = self.train_agents[1].train(batch, self.env, agent._agent_id)
                    agents_losses.append(agent_losses)
            
            if step % 500 == 0 and step > 0:
                self.env.epsilon *= 0.9

            if self.env.merge_count == 8001:
                break
    def save(self):
        save_path = './models/agents_bilevel_1x1_'+str(self.seed)+'.pickle'
        if self.save_path is None:
            self.save_path = './models/agents_bilevel_1x1.pickle'
        with open(save_path, 'wb') as f:
            pickle.dump(self.train_agents, f, pickle.HIGHEST_PROTOCOL)

    def restore(self, restore_path):
        with open(restore_path, 'rb') as f:
            self.train_agents = pickle.load(f)

    def resume(self):
        pass

    def log_diagnostics(self):
        pass

def gambel_softmax(x):
    u = tf.random.uniform(tf.shape(x))
    return tf.nn.softmax(x - tf.math.log(-tf.math.log(u)), axis = -1)

def get_leader_agent(env, agent_id, hidden_layer_sizes, max_replay_buffer_size):
    return LeaderAgent(
        env_specs=env.env_specs,
        policy=StochasticMLPPolicy(
            input_shapes=(env.num_state, ),
            output_shape=(env.action_num, ),
            hidden_layer_sizes=hidden_layer_sizes,
            output_activation=gambel_softmax,
            name='policy_agent_{}'.format(agent_id)
        ),
        qf=MLPValueFunction(
            input_shapes=(env.num_state + env.action_num * 2, ),
            output_shape=(1,),
            hidden_layer_sizes=hidden_layer_sizes,
            name='qf_agent_{}'.format(agent_id)
        ),
        replay_buffer=None,
        # replay_buffer=IndexedReplayBuffer(observation_dim=env.num_state,
        #                                   action_dim=env.action_num,
        #                                   opponent_action_dim=env.agent_num,
        #                                   next_observation_dim = env.num_state,
        #                                   max_replay_buffer_size=max_replay_buffer_size
        #                                   ),

        #exploration_strategy=OUExploration(action_space),
        gradient_clipping=10.,
        agent_id=agent_id,
    )

def get_follower_stochasitc_agent(env, agent_id, hidden_layer_sizes, max_replay_buffer_size):
    return FollowerAgent(
        env_specs=env.env_specs,
        policy=StochasticMLPPolicy(
            input_shapes=(env.num_state + env.action_num, ), # 2 leader 
            output_shape=(env.action_num, ),
            hidden_layer_sizes=hidden_layer_sizes,
            output_activation=gambel_softmax,
            name='policy_agent_{}'.format(agent_id)
        ),
        qf=MLPValueFunction(
            input_shapes=(env.num_state + env.action_num * 2, ),
            output_shape=(1,),
            hidden_layer_sizes=hidden_layer_sizes,
            name='qf_agent_{}'.format(agent_id)
        ),
        replay_buffer=None,
        # replay_buffer=IndexedReplayBuffer(observation_dim=env.num_state + env.action_num,
        #                                   action_dim=env.action_num,
        #                                   opponent_action_dim=env.agent_num,
        #                                   next_observation_dim = env.num_state,
        #                                   max_replay_buffer_size=max_replay_buffer_size
        #                                   ),
        #exploration_strategy=OUExploration(action_space),
        gradient_clipping=10.,
        agent_id=agent_id,
    )

class FakeLeaderAgent:
    def __init__(self, env, agent_id, hidden_layer_sizes, max_replay_buffer_size):
        self.replay_buffer=IndexedReplayBuffer(observation_dim=env.num_state,
                                          action_dim=env.action_num,
                                          opponent_action_dim=env.agent_num,
                                          next_observation_dim = env.num_state,
                                          max_replay_buffer_size=max_replay_buffer_size
                                          )
        self._agent_id = agent_id

class FakeFollowerAgent:
    def __init__(self, env, agent_id, hidden_layer_sizes, max_replay_buffer_size):
        self.replay_buffer=IndexedReplayBuffer(observation_dim=env.num_state + env.action_num,
                                          action_dim=env.action_num,
                                          opponent_action_dim=env.agent_num,
                                          next_observation_dim = env.num_state,
                                          max_replay_buffer_size=max_replay_buffer_size
                                          )
        self._agent_id = agent_id


# for seed in range(0, 10):
if __name__ == '__main__':
    import sys
    seed = int(sys.argv[1])

    print(seed)
    set_seed(seed)

    agent_setting = 'trevor'
    game_name = 'merge_env'
    suffix = f'{game_name}/{agent_setting}/{seed}'

    set_logger(suffix)

    env = gym.make("merge-v0")

    env.agent_num = 2
    env.leader_num = 1
    env.follower_num = 1
    action_num = 5
    batch_size = 64
    training_steps = 500000
    exploration_step = 500
    hidden_layer_sizes = (30, 30)
    max_replay_buffer_size = 500


    train_agents = []

    agents = [
        # get_leader_agent(env, 0, hidden_layer_sizes=hidden_layer_sizes, max_replay_buffer_size=max_replay_buffer_size),
        # get_follower_stochasitc_agent(env, 1, hidden_layer_sizes=hidden_layer_sizes, max_replay_buffer_size=max_replay_buffer_size),
        FakeLeaderAgent(env, 0, hidden_layer_sizes=hidden_layer_sizes, max_replay_buffer_size=max_replay_buffer_size),
        FakeFollowerAgent(env, 1, hidden_layer_sizes=hidden_layer_sizes, max_replay_buffer_size=max_replay_buffer_size),
    ]

    train_agents = [
        get_leader_agent(env, 0, hidden_layer_sizes=hidden_layer_sizes, max_replay_buffer_size=max_replay_buffer_size),
        get_follower_stochasitc_agent(env, 1, hidden_layer_sizes=hidden_layer_sizes, max_replay_buffer_size=max_replay_buffer_size),
    ]

    sampler = MASampler(env.agent_num, env.leader_num, env.follower_num)
    sampler.initialize(env, agents, train_agents)

    trainer = Bilevel_Trainer(
        seed=seed, env=env, agents=agents, train_agents=train_agents, sampler=sampler,
        steps=training_steps, exploration_steps=exploration_step,
        extra_experiences=['target_actions'], batch_size=batch_size
    )

    trainer.run()
    # trainer.save()